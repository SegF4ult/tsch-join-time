#!/usr/bin/env python

from glob import glob
from os import getcwd
from os.path import join, basename, dirname
from subprocess import Popen, PIPE

import re

DATA_FILE_EXT = "*.log"
SET_FILE_EXT = ".set"

def process_file(filename):
    # Run tail to read the file and skip the first 27 lines
    tailp = Popen(["tail", "-n", "+27", filename], stdout=PIPE)
    # Capture output from the process
    (output, err) = tailp.communicate()

    LOG_RE = re.compile(r"^\[(?P<TIME>\d+)\s+(?P<LEVEL>\w+\s*):(?P<MODULE>[\w\/]+\s*\w*)\s*\]")

    # Split the output
    lines = output.splitlines()
    # Convert to str type
    lines = [line.decode("utf-8",'ignore') for line in lines]
    # Filter out picocom lines
    lines = [line for line in lines if (LOG_RE.match(line) or line.startswith("Clock"))]

    tps = 0
    start = 999
    end = 0
    # Run through all lines and parse data
    for line in lines:
        log_result = LOG_RE.match(line)
        if not log_result == None:
            line_contents = re.sub(LOG_RE, "", line).lstrip()
            module = log_result.group("MODULE").strip()
            log_level = log_result.group("LEVEL").strip()
            if module == "TSCH" and log_level == "INFO":
                if line_contents.startswith("starting"):
                    start = int(log_result.group("TIME"))
                if line_contents.startswith("scan:"):
                    end = int(log_result.group("TIME"))
                    break
        else:
            tps = int(re.search("(?P<TPS>\d+)", line).group("TPS"))
    return (tps,start,end)
            

def getFilelist():
    return glob(join(getcwd(), DATA_FILE_EXT))

if __name__ == "__main__":
    file_list = getFilelist()
    data = []
    data_dir = None
    set_name = None
    for f in file_list:
        if set_name == None or data_dir == None:
            data_dir = dirname(f)
            set_name = basename(data_dir)
        file_data = process_file(f)
        duration = (file_data[2] - file_data[1])/file_data[0]
        data.append(duration)

    with open(join(dirname(data_dir), set_name + SET_FILE_EXT), 'w') as set_file:
        for item in data:
            set_file.write("%s\n" % str(item))
