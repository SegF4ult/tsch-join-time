#!/usr/bin/env python

from glob import glob
from os import getcwd
from os.path import join, basename
import matplotlib.pyplot as plt

FILE_EXT="*.set"

def print_usage():
    print("Usage: generateGraph.py")
    print("Please note, it must be run in a folder that contains *.set files")

def process_set_file(filename):
    set_data = []
    for line in open(filename, 'r'):
        set_data.append(float(line))
    return set_data

def getFilelist():
    return glob(join(getcwd(), FILE_EXT))

if __name__ == "__main__":
    file_list = getFilelist()
    data = {}

    if len(file_list) <= 0:
        print_usage()
        exit()

    for f in file_list:
        set_name = basename(f).split(".")[0]
        file_data = process_set_file(f)
        data[set_name] = file_data

    plot_data = []
    plot_labels = []
    for dataset in sorted(data.keys()):
        print(dataset)
        ## join-times-{TAG}-{CHS}-{PRP}.set
        elements=dataset.split("-")
        TAG=elements[2]
        CHS=elements[3]
        PRP=elements[4]
        print(PRP)
        plot_labels.append(PRP)
        plot_data.append(data[dataset])

    graphtitle=""
    filename=TAG+"-"+CHS

    plt.boxplot(plot_data, patch_artist=True, labels=plot_labels)
    plt.ylabel("Association Time (s)")
    plt.xlabel("Packet Reception Rate (%)")
    plt.ylim((0, 300))
    #plt.title(graphtitle)
    plt.savefig(filename+".png")
