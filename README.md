# TSCH Join Time

This repository hosts code, as well as the experiments and data for my evaluation and possible improvement of join times in the TSCH MAC protocol.

## Repository Layout

In the Contiki folder is all the code used for my physical experiments using Contiki-NG on a Ti Launchpad CC1350-XL.

In the Simulator folder you'll find a simulator written in python to verify and test the hypothesis behind my experimentation.
