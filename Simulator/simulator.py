#!/usr/bin/env python
import random
from hopping import *
from experiments import *

########################################
## Simulator main class
########################################
class Simulator(object):
	"""A simulator that models the joining process for a single leaf node and a coordinator.
	   The Simulator has the following properties:

	Attributes:
		experiment: An Experiment object, which stores the parameters needed for the experiment.
		hop_seq: A list which represents the channel hopping sequence for the coordinator node.
		scan_seq: A list which represents the channels that the leaf node will scan for the EB packet.
		results: A list of time results from all the Simulator runs, given in seconds.
	"""
	# Experiment parameters
	experiment=None

	# Node parameters initial state
	hop_seq = []
	scan_seq = []

	# Simulator results
	results = []

	def __init__(self, experiment):
		"""Returns a new instance of Simulator with the given experiment parameters initialized."""
		self.experiment = experiment
		self.hop_seq = experiment.get_hop_sequence()
		self.hop_seq_orig = self.hop_seq

	def run(self):
		e = self.experiment
		self.hop_seq = self.hop_seq_orig
		self.scan_seq = e.get_scan_sequence()
		scan_seq_len = len(self.scan_seq)

		for asn in range(1, e.asn_final):
			if asn % e.slotframe_length == 0:
				eb_ch = self.hop_seq[asn % e.hop_seq_length]
				scan_ch = self.scan_seq[int((asn / e.scanning_window_length)) % scan_seq_len]
				if eb_ch == scan_ch:
					if random.random() < e.packet_reception_probability:
						join_time = (asn * e.timeslot_length) / 1000.0
						self.results.append(join_time)
						break
