#!/usr/bin/env python
import random
from simulator import Simulator
from experiments import *
from hopping import *

#experiment_types = [ Experiment, ExperimentMemorization, ExperimentMemorizationOutdated ]
experiment_types = [ ExperimentMemorizationOutdated ]
#chs_lengths = [ 14, 12, 10, 8, 6 ]
chs_lengths = [ 14 ]
#prp_values = [ 0.8, 0.6, 0.4, 0.2 ]
prp_values = [ 0.8 ]

if __name__ == "__main__":
	for e_type in experiment_types:
		tag = e_type.get_tag()
		for prp in prp_values:
			random.seed()
			prp_file_suffix = str(int(prp*100))
			for chs_len in chs_lengths:
				chs_len_file_suffix = str(chs_len)
				filename = "join-times-" \
							+tag+"-" \
							+chs_len_file_suffix+"-" \
							+prp_file_suffix+".set"
				print("=== ", chs_len, " ", prp, " ===")
				hsg = RandomHoppingSequenceGenerator(chs_len)
				exp = e_type(hsg, prp)
				print(filename)
				sim = Simulator(exp)
				for _ in range(10000):
					sim.run()
				with open(filename, "w+") as filehandle:
					for time in sim.results:
						filehandle.write(str(time))
						filehandle.write("\n")
