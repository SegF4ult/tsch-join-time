#!/usr/bin/env python3
import random

########################################
## Sequence Generator Classes
########################################
class HoppingSequenceGenerator:
	def __init__(self, length):
		self.length = length

	def get_sequence(self):
		pass

class RandomHoppingSequenceGenerator(HoppingSequenceGenerator):
	def get_sequence(self):
		seq = list(range(11,27))
		random.shuffle(seq)
		return seq[:self.length]

class Lfsr:
	def __init__(self, numbits, taps, seed):
		self.numbits = numbits
		self.taps = sorted(taps)
		self.seed = seed

		self.shiftreg = self.int2reg(self.seed)

	def iter(self):
		carrybit = self.shiftreg[-1]
		for tap in sorted(self.taps, reverse=True):
			if tap == self.numbits:
				continue
			carrybit ^= self.shiftreg[tap-1]

		self.shiftreg = self.shiftreg[:-1]
		self.shiftreg.insert(0, carrybit)

	def int2reg(self, intNum):
		returnVal  = [0]*self.numbits
		for i in range(self.numbits):
			if intNum & (1<<i):
				returnVal[i]=1
		return returnVal

	def reg2int(self):
		returnVal = 0
		for i in range(self.numbits):
			returnVal += self.shiftreg[i]<<i
		return returnVal

class DefaultHoppingSequenceGenerator(HoppingSequenceGenerator):
	"""Implementation based on: https://gist.github.com/twatteyne/2e22ee3c1a802b685695"""

	def get_sequence(self):
		lfsr = Lfsr(9, [5, 9], 255)
		var_SHUFFLE = []
		for c in range(self.length):
			lfsr.iter()
			var_SHUFFLE += [lfsr.reg2int()%self.length] 
		var_CHANNELS = list(range(self.length))

		for i in range(self.length):
			chan_i = var_CHANNELS[i]
			chan_shuffle_i = var_CHANNELS[var_SHUFFLE[i]]

			var_CHANNELS[i] = chan_shuffle_i
			var_CHANNELS[var_SHUFFLE[i]]	= chan_i

		var_CHANNELS = [x + 11 for x in var_CHANNELS]
		return var_CHANNELS
