#!/usr/bin/env python3
import random
from hopping import HoppingSequenceGenerator

########################################
## Experiment classes
########################################
class Experiment:
	"""An Experiment for the TSCH Join-time Simulator to run.
	Experiments have the following properties:

	Attributes:
		asn_final: integer, the total amount of timeslots to run the simulation for.
		slotframe_length: integer, the amount of timeslots in a slotframe.
		scanning_window_length: integer, the length of the scanning window in timeslots.
		timeslot_length: integer, the length of a timeslot in milliseconds.
	"""
	# Simulation parameters
	asn_final=1000000					   # Amount of timeslots to run the simulation for
	slotframe_length=101					# Amount of timeslots in a slotframe
	scanning_window_length = 17			 # Length of scanning window (in timeslots)
	timeslot_length = 10					# Length of a timeslot in ms

	hop_seq_generator = None

	# Tag used in filename generation to distinguish different experiment types
	tag = "base"

	def __init__(self, seq_gen, prp=0.8):
		if not isinstance(seq_gen, HoppingSequenceGenerator):
			raise TypeError
		self.hop_seq_generator = seq_gen
		self.hop_seq_length = seq_gen.length
		self.packet_reception_probability = prp
		pass

	## Packet Reception Probability
	def set_packet_reception_probability(self, prp):
		self.packet_reception_probability = prp

	def get_packet_reception_probability(self):
		return self.packet_reception_probability

	## Hopping Sequence Length
	def set_hop_sequence_length(self, length):
		self.hop_seq_length = length

	def get_hop_sequence_length(self):
		return self.seq_gen.length

	## Hopping Sequence
	def get_hop_sequence(self):
		return self.hop_seq_generator.get_sequence()

	## Scanning Sequence
	def get_scan_sequence(self):
		scan_seq = list(range(11,26))
		rotate = random.randint(0, len(scan_seq))
		scan_seq = scan_seq[rotate:] + scan_seq[:rotate]
		return scan_seq

	@classmethod
	def get_tag(cls):
		return cls.tag


class ExperimentMemorization(Experiment):
	# Tag used in filename generation to distinguish different experiment types
	tag = "mem"
	def get_hop_sequence(self):
		self.hop_seq = self.hop_seq_generator.get_sequence()
		return self.hop_seq

	def get_scan_sequence(self):
		# Copy hop sequence into the scan sequence
		self.scan_seq = self.hop_seq
		
		# Generate a random offset
		# this simulates the timephase difference between coordinator and leaf node
		rotate = random.randint(0, len(self.scan_seq))
		self.scan_seq = self.scan_seq[rotate:] + self.scan_seq[:rotate]
		return self.scan_seq

class ExperimentMemorizationOutdated(ExperimentMemorization):
	# Tag used in filename generation to distinguish different experiment types
	tag = "memold"

	def get_scan_sequence(self):
		# Copy scan sequence
		self.scan_seq = self.hop_seq

		# Choose random slot in the sequence to mutate
		cidx = random.randint(0, len(self.hop_seq)-1)
		old_ch = self.hop_seq[cidx]

		# Generate a new channel number
		while True:
			new_ch = random.randint(11, 26)
			if new_ch != old_ch and new_ch not in self.hop_seq:
				self.hop_seq[cidx] = new_ch
				break
			
		# Generate a random offset
		# this simulates the timephase difference between coordinator and leaf node
		rotate = random.randint(0, len(self.scan_seq))
		self.scan_seq = self.scan_seq[rotate:] + self.scan_seq[:rotate]
		return self.scan_seq
