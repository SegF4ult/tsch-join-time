/**
 * \file
 *         A simple Contiki application to
 * \author
 *         Xander G. Bos <xander.g.bos@gmail.com>
 *
 */

#include "contiki.h"
#include "net/mac/tsch/tsch.h"
#include "net/routing/routing.h"
#include "sys/log.h"
#include "sys/timer.h"

#include <stdio.h>

void LOG_PREFIX_TIMESTAMPED(int level, char* levelstr, char* module) {
	LOG_OUTPUT("[%ld %-4s:%-8s] ", clock_time(), levelstr, module);
}

/*----------------------------------------------------------------------*/
PROCESS(joining_experiment_process, "Joining experiment process");
AUTOSTART_PROCESSES(&joining_experiment_process);

PROCESS_THREAD(joining_experiment_process, ev, data)
{
	PROCESS_BEGIN();

	/* Set up as TSCH coordinator and start the TSCH services */
	NETSTACK_ROUTING.root_start();
	NETSTACK_MAC.on();

	printf("Clock ticks per second: %d\n", CLOCK_SECOND);
	PROCESS_END();
}
