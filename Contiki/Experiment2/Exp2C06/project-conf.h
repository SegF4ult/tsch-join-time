/*
 * TSCH Config
 */
#define TSCH_CONF_AUTOSTART                     0

#define TSCH_CONF_JOIN_SECURED_ONLY             0
#define TSCH_SCHEDULE_CONF_DEFAULT_LENGTH       7

#define TSCH_HOPPING_SEQUENCE_10                (uint8_t[]){ 16, 12, 19, 13, 17, 14, 20, 18, 15, 11 }
#define TSCH_HOPPING_SEQUENCE_8                 (uint8_t[]){ 16, 12, 15, 11, 14, 13, 17, 18 }
#define TSCH_HOPPING_SEQUENCE_6                 (uint8_t[]){ 16, 12, 15, 11, 13, 14  }

#define TSCH_CONF_DEFAULT_HOPPING_SEQUENCE      TSCH_HOPPING_SEQUENCE_6

/* 
 * Logging Config
 */
#define LOG_CONF_LEVEL_TCPIP                    LOG_LEVEL_DBG
#define LOG_CONF_LEVEL_MAC                      LOG_LEVEL_DBG

void LOG_PREFIX_TIMESTAMPED(int, char*, char*);
#define LOG_CONF_OUTPUT_PREFIX                  LOG_PREFIX_TIMESTAMPED
